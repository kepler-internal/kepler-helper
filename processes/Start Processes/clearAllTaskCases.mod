[Ivy]
[>Created: Mon May 22 16:06:39 ICT 2017]
162D1DC26ABF167B 3.18 #module
>Proto >Proto Collection #zClass
cs0 clearAllTaskCases Big #zClass
cs0 B #cInfo
cs0 #process
cs0 @TextInP .resExport .resExport #zField
cs0 @TextInP .type .type #zField
cs0 @TextInP .processKind .processKind #zField
cs0 @AnnotationInP-0n ai ai #zField
cs0 @MessageFlowInP-0n messageIn messageIn #zField
cs0 @MessageFlowOutP-0n messageOut messageOut #zField
cs0 @TextInP .xml .xml #zField
cs0 @TextInP .responsibility .responsibility #zField
cs0 @StartRequest f0 '' #zField
cs0 @EndTask f1 '' #zField
cs0 @RichDialog f5 '' #zField
cs0 @PushWFArc f2 '' #zField
cs0 @PushWFArc f7 '' #zField
>Proto cs0 cs0 clearAllTaskCases #zField
cs0 f0 outLink start.ivp #txt
cs0 f0 type ch.axonivy.finform.desk_post_test.clearAllTaskCasesData #txt
cs0 f0 inParamDecl '<> param;' #txt
cs0 f0 actionDecl 'ch.axonivy.finform.desk_post_test.clearAllTaskCasesData out;
' #txt
cs0 f0 guid 15C10B03989D8005 #txt
cs0 f0 requestEnabled true #txt
cs0 f0 triggerEnabled false #txt
cs0 f0 callSignature start() #txt
cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
cs0 f0 @C|.responsibility Everybody #txt
cs0 f0 81 49 30 30 -21 17 #rect
cs0 f0 @|StartRequestIcon #fIcon
cs0 f1 type ch.axonivy.finform.desk_post_test.clearAllTaskCasesData #txt
cs0 f1 385 49 30 30 0 15 #rect
cs0 f1 @|EndIcon #fIcon
cs0 f5 targetWindow NEW:card: #txt
cs0 f5 targetDisplay TOP #txt
cs0 f5 richDialogId ch.axonivy.finform.desk_post_test.ConfirmDelete #txt
cs0 f5 startMethod start() #txt
cs0 f5 type ch.axonivy.finform.desk_post_test.clearAllTaskCasesData #txt
cs0 f5 requestActionDecl '<> param;' #txt
cs0 f5 responseActionDecl 'ch.axonivy.finform.desk_post_test.clearAllTaskCasesData out;
' #txt
cs0 f5 responseMappingAction 'out=in;
' #txt
cs0 f5 windowConfiguration '* ' #txt
cs0 f5 isAsynch false #txt
cs0 f5 isInnerRd false #txt
cs0 f5 userContext '* ' #txt
cs0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>confirm delete</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
cs0 f5 192 42 112 44 -39 -8 #rect
cs0 f5 @|RichDialogIcon #fIcon
cs0 f2 expr out #txt
cs0 f2 304 64 385 64 #arcP
cs0 f7 expr out #txt
cs0 f7 111 64 192 64 #arcP
>Proto cs0 .type ch.axonivy.finform.desk_post_test.clearAllTaskCasesData #txt
>Proto cs0 .processKind NORMAL #txt
>Proto cs0 0 0 32 24 18 0 #rect
>Proto cs0 @|BIcon #fIcon
cs0 f5 mainOut f2 tail #connect
cs0 f2 head f1 mainIn #connect
cs0 f0 mainOut f7 tail #connect
cs0 f7 head f5 mainIn #connect
