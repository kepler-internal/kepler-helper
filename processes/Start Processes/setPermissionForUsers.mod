[Ivy]
162D1DEC65C2DE02 3.20 #module
>Proto >Proto Collection #zClass
ss0 setPermissionForUsers Big #zClass
ss0 B #cInfo
ss0 #process
ss0 @TextInP .resExport .resExport #zField
ss0 @TextInP .type .type #zField
ss0 @TextInP .processKind .processKind #zField
ss0 @AnnotationInP-0n ai ai #zField
ss0 @MessageFlowInP-0n messageIn messageIn #zField
ss0 @MessageFlowOutP-0n messageOut messageOut #zField
ss0 @TextInP .xml .xml #zField
ss0 @TextInP .responsibility .responsibility #zField
ss0 @StartRequest f0 '' #zField
ss0 @EndTask f1 '' #zField
ss0 @GridStep f3 '' #zField
ss0 @PushWFArc f4 '' #zField
ss0 @PushWFArc f2 '' #zField
>Proto ss0 ss0 setPermissionForUsers #zField
ss0 f0 outLink start.ivp #txt
ss0 f0 type ch.axonivy.finform.desk_post_test.setPermissionForUsersData #txt
ss0 f0 inParamDecl '<> param;' #txt
ss0 f0 actionDecl 'ch.axonivy.finform.desk_post_test.setPermissionForUsersData out;
' #txt
ss0 f0 guid 1626677D0521FF27 #txt
ss0 f0 requestEnabled true #txt
ss0 f0 triggerEnabled false #txt
ss0 f0 callSignature start() #txt
ss0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
ss0 f0 @C|.responsibility Everybody #txt
ss0 f0 81 49 30 30 -21 17 #rect
ss0 f0 @|StartRequestIcon #fIcon
ss0 f1 type ch.axonivy.finform.desk_post_test.setPermissionForUsersData #txt
ss0 f1 337 49 30 30 0 15 #rect
ss0 f1 @|EndIcon #fIcon
ss0 f3 actionDecl 'ch.axonivy.finform.desk_post_test.setPermissionForUsersData out;
' #txt
ss0 f3 actionTable 'out=in;
' #txt
ss0 f3 actionCode 'import ch.axonivy.finform.common.service.PermissionService;
import ch.ivyteam.ivy.security.IRole;
import ch.ivyteam.ivy.security.IUser;
import ch.ivyteam.ivy.security.IPermission;

IPermission permission = IPermission.DOCUMENT_READ;

// Grant Permission to role BankEmployee
IRole bankEmployeeRole = ivy.wf.getSecurityContext().findRole("BankEmployee");

ivy.wf.getApplication().getSecurityDescriptor().grantPermission(IPermission.TASK_DESTROY, bankEmployeeRole);
PermissionService.setPermissionsForUserRole(PermissionService.DOCUMENT_PERMISSIONS, bankEmployeeRole);
PermissionService.setPermissionsForUserRole(PermissionService.TASK_CUSTOM_FIELD_PERMISSIONS, bankEmployeeRole);

// Grant Permission to role ComplianceOfficer
IRole complianceOfficerRole = ivy.wf.getSecurityContext().findRole("ComplianceOfficer");

PermissionService.setPermissionsForUserRole(PermissionService.DOCUMENT_PERMISSIONS, complianceOfficerRole);
PermissionService.setPermissionsForUserRole(PermissionService.TASK_CUSTOM_FIELD_PERMISSIONS, complianceOfficerRole);' #txt
ss0 f3 security system #txt
ss0 f3 type ch.axonivy.finform.desk_post_test.setPermissionForUsersData #txt
ss0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set users permssion</name>
        <nameStyle>19,7
</nameStyle>
    </language>
</elementInfo>
' #txt
ss0 f3 160 42 128 44 -57 -8 #rect
ss0 f3 @|StepIcon #fIcon
ss0 f4 expr out #txt
ss0 f4 111 64 160 64 #arcP
ss0 f2 expr out #txt
ss0 f2 288 64 337 64 #arcP
>Proto ss0 .type ch.axonivy.finform.desk_post_test.setPermissionForUsersData #txt
>Proto ss0 .processKind NORMAL #txt
>Proto ss0 0 0 32 24 18 0 #rect
>Proto ss0 @|BIcon #fIcon
ss0 f0 mainOut f4 tail #connect
ss0 f4 head f3 mainIn #connect
ss0 f3 mainOut f2 tail #connect
ss0 f2 head f1 mainIn #connect
