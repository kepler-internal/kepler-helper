[Ivy]
[>Created: Fri Jan 05 09:48:05 ICT 2018]
160C424AA1D8C003 3.18 #module
>Proto >Proto Collection #zClass
gg0 getApplicationLog Big #zClass
gg0 B #cInfo
gg0 #process
gg0 @TextInP .resExport .resExport #zField
gg0 @TextInP .type .type #zField
gg0 @TextInP .processKind .processKind #zField
gg0 @AnnotationInP-0n ai ai #zField
gg0 @MessageFlowInP-0n messageIn messageIn #zField
gg0 @MessageFlowOutP-0n messageOut messageOut #zField
gg0 @TextInP .xml .xml #zField
gg0 @TextInP .responsibility .responsibility #zField
gg0 @StartRequest f0 '' #zField
gg0 @GridStep f3 '' #zField
gg0 @PushWFArc f4 '' #zField
>Proto gg0 gg0 getApplicationLog #zField
gg0 f0 outLink start.ivp #txt
gg0 f0 type ch.axonivy.finform.logging.Data #txt
gg0 f0 inParamDecl '<> param;' #txt
gg0 f0 actionDecl 'ch.axonivy.finform.logging.Data out;
' #txt
gg0 f0 guid 160C424AA22EA458 #txt
gg0 f0 requestEnabled true #txt
gg0 f0 triggerEnabled false #txt
gg0 f0 callSignature start() #txt
gg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
gg0 f0 @C|.responsibility Everybody #txt
gg0 f0 81 49 30 30 -21 17 #rect
gg0 f0 @|StartRequestIcon #fIcon
gg0 f3 actionDecl 'ch.axonivy.finform.logging.Data out;
' #txt
gg0 f3 actionTable 'out=in;
' #txt
gg0 f3 actionCode 'import ch.axonivy.finform.common.service.LoggingService;
LoggingService.downloadLogFile();' #txt
gg0 f3 security system #txt
gg0 f3 type ch.axonivy.finform.logging.Data #txt
gg0 f3 168 42 112 44 0 -8 #rect
gg0 f3 @|StepIcon #fIcon
gg0 f4 expr out #txt
gg0 f4 111 64 168 64 #arcP
>Proto gg0 .type ch.axonivy.finform.logging.Data #txt
>Proto gg0 .processKind NORMAL #txt
>Proto gg0 0 0 32 24 18 0 #rect
>Proto gg0 @|BIcon #fIcon
gg0 f0 mainOut f4 tail #connect
gg0 f4 head f3 mainIn #connect
