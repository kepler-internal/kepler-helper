[Ivy]
[>Created: Tue May 16 16:56:08 ICT 2017]
162D1DE7C968DF1B 3.18 #module
>Proto >Proto Collection #zClass
ie0 invalidateAllRuleCache Big #zClass
ie0 B #cInfo
ie0 #process
ie0 @TextInP .resExport .resExport #zField
ie0 @TextInP .type .type #zField
ie0 @TextInP .processKind .processKind #zField
ie0 @AnnotationInP-0n ai ai #zField
ie0 @MessageFlowInP-0n messageIn messageIn #zField
ie0 @MessageFlowOutP-0n messageOut messageOut #zField
ie0 @TextInP .xml .xml #zField
ie0 @TextInP .responsibility .responsibility #zField
ie0 @GridStep f3 '' #zField
ie0 @StartRequest f4 '' #zField
ie0 @EndTask f5 '' #zField
ie0 @PushWFArc f6 '' #zField
ie0 @PushWFArc f7 '' #zField
>Proto ie0 ie0 invalidateAllRuleCache #zField
ie0 f3 actionDecl 'ch.axonivy.finform.desk_post_test.invalidateAllRuleCacheData out;
' #txt
ie0 f3 actionTable 'out=in;
' #txt
ie0 f3 actionCode 'import ch.axonivy.fintech.standard.guiframework.util.RuleStore;
RuleStore.getInstance().invalidCachedRuleBases();' #txt
ie0 f3 type ch.axonivy.finform.desk_post_test.invalidateAllRuleCacheData #txt
ie0 f3 170 63 112 44 0 -8 #rect
ie0 f3 @|StepIcon #fIcon
ie0 f4 outLink start.ivp #txt
ie0 f4 type ch.axonivy.finform.desk_post_test.invalidateAllRuleCacheData #txt
ie0 f4 inParamDecl '<> param;' #txt
ie0 f4 actionDecl 'ch.axonivy.finform.desk_post_test.invalidateAllRuleCacheData out;
' #txt
ie0 f4 guid 15C10AF5F1FBBD3F #txt
ie0 f4 requestEnabled true #txt
ie0 f4 triggerEnabled false #txt
ie0 f4 callSignature start() #txt
ie0 f4 persist false #txt
ie0 f4 startName 'Clear all rule cache' #txt
ie0 f4 taskData 'TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody' #txt
ie0 f4 showInStartList 1 #txt
ie0 f4 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();
import ch.ivyteam.ivy.request.impl.DefaultCalendarProxy;
DefaultCalendarProxy calendarProxy = ivy.cal as DefaultCalendarProxy;
taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
ie0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
    </language>
</elementInfo>
' #txt
ie0 f4 @C|.responsibility Everybody #txt
ie0 f4 83 70 30 30 -21 17 #rect
ie0 f4 @|StartRequestIcon #fIcon
ie0 f5 type ch.axonivy.finform.desk_post_test.invalidateAllRuleCacheData #txt
ie0 f5 339 70 30 30 0 15 #rect
ie0 f5 @|EndIcon #fIcon
ie0 f6 expr out #txt
ie0 f6 282 85 339 85 #arcP
ie0 f7 expr out #txt
ie0 f7 113 85 170 85 #arcP
>Proto ie0 .type ch.axonivy.finform.desk_post_test.invalidateAllRuleCacheData #txt
>Proto ie0 .processKind NORMAL #txt
>Proto ie0 0 0 32 24 18 0 #rect
>Proto ie0 @|BIcon #fIcon
ie0 f4 mainOut f7 tail #connect
ie0 f7 head f3 mainIn #connect
ie0 f3 mainOut f6 tail #connect
ie0 f6 head f5 mainIn #connect
