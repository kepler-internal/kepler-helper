[Ivy]
[>Created: Mon May 22 16:06:10 ICT 2017]
162D1DD1F7239B45 3.18 #module
>Proto >Proto Collection #zClass
Cs0 ConfirmDeleteProcess Big #zClass
Cs0 RD #cInfo
Cs0 #process
Cs0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Cs0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Cs0 @TextInP .resExport .resExport #zField
Cs0 @TextInP .type .type #zField
Cs0 @TextInP .processKind .processKind #zField
Cs0 @AnnotationInP-0n ai ai #zField
Cs0 @MessageFlowInP-0n messageIn messageIn #zField
Cs0 @MessageFlowOutP-0n messageOut messageOut #zField
Cs0 @TextInP .xml .xml #zField
Cs0 @TextInP .responsibility .responsibility #zField
Cs0 @RichDialogInitStart f0 '' #zField
Cs0 @RichDialogProcessEnd f1 '' #zField
Cs0 @PushWFArc f2 '' #zField
Cs0 @RichDialogProcessStart f3 '' #zField
Cs0 @RichDialogEnd f4 '' #zField
Cs0 @PushWFArc f5 '' #zField
Cs0 @RichDialogProcessStart f6 '' #zField
Cs0 @RichDialogEnd f7 '' #zField
Cs0 @GridStep f9 '' #zField
Cs0 @PushWFArc f10 '' #zField
Cs0 @PushWFArc f8 '' #zField
>Proto Cs0 Cs0 ConfirmDeleteProcess #zField
Cs0 f0 guid 15C2F641D4728A3C #txt
Cs0 f0 type ch.axonivy.finform.desk_post_test.ConfirmDelete.ConfirmDeleteData #txt
Cs0 f0 method start() #txt
Cs0 f0 disableUIEvents true #txt
Cs0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Cs0 f0 outParameterDecl '<> result;
' #txt
Cs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
    </language>
</elementInfo>
' #txt
Cs0 f0 83 51 26 26 -16 15 #rect
Cs0 f0 @|RichDialogInitStartIcon #fIcon
Cs0 f1 type ch.axonivy.finform.desk_post_test.ConfirmDelete.ConfirmDeleteData #txt
Cs0 f1 334 51 26 26 0 12 #rect
Cs0 f1 @|RichDialogProcessEndIcon #fIcon
Cs0 f2 expr out #txt
Cs0 f2 109 64 334 64 #arcP
Cs0 f3 guid 15C2F641D724FDB7 #txt
Cs0 f3 type ch.axonivy.finform.desk_post_test.ConfirmDelete.ConfirmDeleteData #txt
Cs0 f3 actionDecl 'ch.axonivy.finform.desk_post_test.ConfirmDelete.ConfirmDeleteData out;
' #txt
Cs0 f3 actionTable 'out=in;
' #txt
Cs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
    </language>
</elementInfo>
' #txt
Cs0 f3 83 147 26 26 -15 12 #rect
Cs0 f3 @|RichDialogProcessStartIcon #fIcon
Cs0 f4 type ch.axonivy.finform.desk_post_test.ConfirmDelete.ConfirmDeleteData #txt
Cs0 f4 guid 15C2F641D7297437 #txt
Cs0 f4 334 147 26 26 0 12 #rect
Cs0 f4 @|RichDialogEndIcon #fIcon
Cs0 f5 expr out #txt
Cs0 f5 109 160 334 160 #arcP
Cs0 f6 guid 15C2F671B9A34A9A #txt
Cs0 f6 type ch.axonivy.finform.desk_post_test.ConfirmDelete.ConfirmDeleteData #txt
Cs0 f6 actionDecl 'ch.axonivy.finform.desk_post_test.ConfirmDelete.ConfirmDeleteData out;
' #txt
Cs0 f6 actionTable 'out=in;
' #txt
Cs0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>delete</name>
        <nameStyle>6,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f6 83 279 26 26 -17 15 #rect
Cs0 f6 @|RichDialogProcessStartIcon #fIcon
Cs0 f7 type ch.axonivy.finform.desk_post_test.ConfirmDelete.ConfirmDeleteData #txt
Cs0 f7 guid 15C2F641D7297437 #txt
Cs0 f7 334 279 26 26 0 12 #rect
Cs0 f7 @|RichDialogEndIcon #fIcon
Cs0 f9 actionDecl 'ch.axonivy.finform.desk_post_test.ConfirmDelete.ConfirmDeleteData out;
' #txt
Cs0 f9 actionTable 'out=in;
' #txt
Cs0 f9 actionCode 'import ch.ivyteam.ivy.Advisor;

Advisor.getAdvisor().setTesting(true);
ivy.wf.deleteAllCases();
ivy.log.warn("All tasks/ cases have been deleted!!!!");
Advisor.getAdvisor().setTesting(false);' #txt
Cs0 f9 security system #txt
Cs0 f9 type ch.axonivy.finform.desk_post_test.ConfirmDelete.ConfirmDeleteData #txt
Cs0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>delete </name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
Cs0 f9 174 270 112 44 -18 -8 #rect
Cs0 f9 @|StepIcon #fIcon
Cs0 f10 expr out #txt
Cs0 f10 109 292 174 292 #arcP
Cs0 f8 expr out #txt
Cs0 f8 286 292 334 292 #arcP
>Proto Cs0 .type ch.axonivy.finform.desk_post_test.ConfirmDelete.ConfirmDeleteData #txt
>Proto Cs0 .processKind HTML_DIALOG #txt
>Proto Cs0 -8 -8 16 16 16 26 #rect
>Proto Cs0 '' #fIcon
Cs0 f0 mainOut f2 tail #connect
Cs0 f2 head f1 mainIn #connect
Cs0 f3 mainOut f5 tail #connect
Cs0 f5 head f4 mainIn #connect
Cs0 f6 mainOut f10 tail #connect
Cs0 f10 head f9 mainIn #connect
Cs0 f9 mainOut f8 tail #connect
Cs0 f8 head f7 mainIn #connect
