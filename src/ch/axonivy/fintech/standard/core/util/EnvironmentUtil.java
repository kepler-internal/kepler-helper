package ch.axonivy.fintech.standard.core.util;

import java.util.Locale;

import javax.faces.context.FacesContext;

import ch.ivyteam.ivy.application.IApplication;
import ch.ivyteam.ivy.environment.Ivy;

public class EnvironmentUtil {
	public static boolean isIvyEnvironment(){
		return ch.ivyteam.ivy.request.metadata.MetaData.getRequest() != null;
	}
	
	public static boolean isIvyEngine(){
		return !Ivy.wf().getApplication().getName().equals(IApplication.DESIGNER_APPLICATION_NAME);
	}
	@SuppressWarnings("unchecked")
	public static <T> T getGlobalVarOrReturnDefault(String key, T defaultValue){
		if(EnvironmentUtil.isIvyEnvironment()){
			return (T) Ivy.var().get(key);
		}
		return defaultValue;
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public static <T> T getContentObjectValueOrReturnDefault(String key, Locale locale, T defaultValue){
		if(EnvironmentUtil.isIvyEnvironment()){
			return (T) Ivy.cms().getContentObjectValue(key, locale, true).getContentAsString();
		}
		return defaultValue;
	}
	
	public static boolean isJsfEnvironment(){
		return FacesContext.getCurrentInstance() != null;
	}

}
