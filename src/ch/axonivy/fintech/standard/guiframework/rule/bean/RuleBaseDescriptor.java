package ch.axonivy.fintech.standard.guiframework.rule.bean;

import java.util.List;

import ch.ivyteam.ivy.rule.engine.api.IRuleBase;

public class RuleBaseDescriptor {
	private IRuleBase ruleBase;
	private List<String> requireNamespaces;

	public RuleBaseDescriptor(IRuleBase ruleBase,
			List<String> requireNamespaces) {
		this.ruleBase = ruleBase;
		this.requireNamespaces = requireNamespaces;
	}

	public IRuleBase getRuleBase() {
		return ruleBase;
	}
	
	public void setRuleBase(IRuleBase ruleBase) {
		this.ruleBase = ruleBase;
	}
	
	public List<String> getRequireNamespaces() {
		return requireNamespaces;
	}
	
	public void setRequireNamespaces(List<String> requireNamespaces) {
		this.requireNamespaces = requireNamespaces;
	}
	
}
