package ch.axonivy.fintech.standard.guiframework.util;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import ch.axonivy.fintech.standard.core.util.EnvironmentUtil;
import ch.axonivy.fintech.standard.guiframework.rule.bean.RuleBaseDescriptor;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.rule.engine.api.IRuleBase;
import ch.ivyteam.ivy.rule.engine.api.Rules;
import ch.ivyteam.ivy.rule.engine.api.runtime.IStatelessRuleSession;

public class RuleStore {
	
	private static RuleStore instance;
	private static final String RULE_GROUP_ID = "ch.axonivy.fintech.standard.rule";
	private static final String RULE_ENTRY_ID = "/";
	private static final String RULE_SESSION = "ch.axonivy.fintech.standard.rule.session";
	
	private RuleStore(){

	}
	
	@SuppressWarnings("unchecked")
	public Map<String, RuleBaseDescriptor> getStore(){
		if(!isStoreInitialized()){
			initStore();
		}
		return (Map<String, RuleBaseDescriptor>) Ivy.datacache().getAppCache().getEntry(RULE_GROUP_ID, RULE_ENTRY_ID).getValue();
	}
	
	private synchronized void initStore() {
		if(!isStoreInitialized()){
			Ivy.datacache().getAppCache().setEntry(RULE_GROUP_ID, RULE_ENTRY_ID, new ConcurrentHashMap<String, IRuleBase>());
		}
	}
	
	private boolean isStoreInitialized() {
		return Ivy.datacache().getAppCache().getEntry(RULE_GROUP_ID, RULE_ENTRY_ID) != null;
	}
	
	public static RuleStore getInstance(){
		if (instance == null){
			createInstance();
		}
		return instance;
	}
	
	private static synchronized void createInstance() {
		if(instance == null){
			instance = new RuleStore();
			
		}
	}

	
	public IStatelessRuleSession getStatelessRuleSession(String ruleSetName, List<String> requiredNamespaces) {
		RuleBaseDescriptor ruleBaseDesc = null;
		IRuleBase ruleBase = null;
		IStatelessRuleSession ruleSession = null;
		Map<String, IStatelessRuleSession> statelessRuleSessionCache = readFromSession();
		if(EnvironmentUtil.isIvyEnvironment()){
			ruleBaseDesc = getStore().get(ruleSetName);
			if(ruleBaseDesc != null && isSameRequiredNamespaces(ruleBaseDesc.getRequireNamespaces(), requiredNamespaces)){
				ruleBase = ruleBaseDesc.getRuleBase();
				ruleSession = statelessRuleSessionCache.get(ruleSetName);
			}
		}

		if (ruleBase == null) {
			ruleBase = Rules.engine().createRuleBase();
			loadRequiredNamespaces(requiredNamespaces, ruleBase);
			ruleBase.loadRulesFromNamespace(ruleSetName);
			ruleBaseDesc = new RuleBaseDescriptor(ruleBase, requiredNamespaces);
			getStore().put(ruleSetName, ruleBaseDesc);
		}
		
		if(ruleSession == null) {
			ruleSession = ruleBase.createSession();
			statelessRuleSessionCache.put(ruleSetName, ruleSession);
		}

		return  ruleSession;
	}

	@SuppressWarnings("unchecked")
	private Map<String, IStatelessRuleSession> readFromSession() {
		Map<String, IStatelessRuleSession> result = (Map<String, IStatelessRuleSession>) Ivy.session().getAttribute(RULE_SESSION);
		if(result == null) {
			result = new ConcurrentHashMap<>();
			Ivy.session().setAttribute(RULE_SESSION, result);
		}
		return result;
	}

	private boolean isSameRequiredNamespaces(List<String> oldRequiredNamespaces, List<String> newRequiredNamespaces) {
		if(CollectionUtils.isNotEmpty(oldRequiredNamespaces) && CollectionUtils.isEmpty(newRequiredNamespaces)){
			return false;
		}
		
		if(CollectionUtils.isEmpty(oldRequiredNamespaces) && CollectionUtils.isNotEmpty(newRequiredNamespaces)){
			return false;
		}
		
		if(CollectionUtils.isNotEmpty(oldRequiredNamespaces) && CollectionUtils.isNotEmpty(newRequiredNamespaces)){
			List<String> sortedOldRequiredNamespaces = oldRequiredNamespaces.stream()
																			.sorted((o1,o2)-> o1.compareToIgnoreCase(o2))
																			.collect(Collectors.toList());
			List<String> sortedNewRequiredNamespaces = newRequiredNamespaces.stream()
																			.sorted((o1,o2)-> o1.compareToIgnoreCase(o2))
																			.collect(Collectors.toList());
			
			return CollectionUtils.containsAll(sortedOldRequiredNamespaces, sortedNewRequiredNamespaces);
		}
		
		return true;
	}

	void loadRequiredNamespaces(List<String> requiredNamespaces, IRuleBase ruleBase) {
		if(CollectionUtils.isNotEmpty(requiredNamespaces)){
			requiredNamespaces.stream().forEachOrdered(item ->{
				ruleBase.loadRulesFromNamespace(item);
			});
		}
	}
	

	public void invalidCachedRuleBases() {
		getStore().clear();
	}
}
