package ch.axonivy.finform.common.service;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;

import javax.servlet.http.HttpServletResponse;

import ch.ivyteam.ivy.environment.EnvironmentNotAvailableException;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.request.IHttpResponse;
import ch.ivyteam.ivy.request.IResponse;

public class LoggingService {
	
	private static final String LOG_PATH = "ch_axonivy_finform_logging_path";
	private static final String LIMITED_LOG_LINE_VIEW_NO = "ch_axonivy_finform_logging_limit_line_view_no";
	
    public static void downloadLogFile() throws EnvironmentNotAvailableException, IOException{
		File file = new File(Ivy.var().get(LOG_PATH));
		
    	if(file.exists()){
    		IResponse response = Ivy.response();
            if (!(response instanceof IHttpResponse)) {
                throw new RuntimeException("Response must be an HTTP response, but was " + response);
            }

            IHttpResponse httpResponse = (IHttpResponse) response;
            HttpServletResponse servletResponse = httpResponse.getHttpServletResponse();
            servletResponse.setContentType("text/plain");
            try {
                OutputStream out = servletResponse.getOutputStream();
                out.write(getLimitedLogFile(file).toString().getBytes());
                out.close();
            } catch (IOException e) {
                Ivy.log().error("Can not download data", e);
            }
    	}
	}
	
	private static String getLimitedLogFile(File file){
		RandomAccessFile randomAccessFile = null;
		StringBuilder builder = new StringBuilder();
		try {
			randomAccessFile = new RandomAccessFile(file, "r");
			long fileLength = randomAccessFile.length() - 1;
			randomAccessFile.seek(fileLength);

			long line = 0;
			
			for(long i = fileLength; i != -1; i--){
				randomAccessFile.seek(i);
				int readByte = randomAccessFile.readByte();
	
	             if( readByte == 0xD || readByte == 0xA ) {
	                if ( i < fileLength ) {
	                    line = line + 1;
	                }
	            }
	             
	            if (line >=  Long.parseLong(Ivy.var().get(LIMITED_LOG_LINE_VIEW_NO))) {
	                break;
	            }
	            builder.append( ( char ) readByte );
			}
		} catch( java.io.FileNotFoundException e ) {
	        e.printStackTrace();
	        return null;
	    } catch( java.io.IOException e ) {
	        e.printStackTrace();
	        return null;
	    } finally {
	        if (randomAccessFile != null )
	            try {
	            	randomAccessFile.close();
	            } catch (IOException e) {
	            }
	    }
		return builder.reverse().toString();
		
	}

}
