package ch.axonivy.finform.common.service;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import ch.ivyteam.ivy.environment.Ivy;
import ch.ivyteam.ivy.security.IPermission;
import ch.ivyteam.ivy.security.IRole;

public final class PermissionService {

	public static final Set<IPermission> TASK_CUSTOM_FIELD_PERMISSIONS = Stream.of(
			IPermission.TASK_READ_CUSTOM_DECIMAL_FIELD1, IPermission.TASK_READ_CUSTOM_DECIMAL_FIELD2,
			IPermission.TASK_READ_CUSTOM_DECIMAL_FIELD3, IPermission.TASK_READ_CUSTOM_DECIMAL_FIELD4,
			IPermission.TASK_READ_CUSTOM_DECIMAL_FIELD5, IPermission.TASK_READ_CUSTOM_TIMESTAMP_FIELD1,
			IPermission.TASK_READ_CUSTOM_TIMESTAMP_FIELD2, IPermission.TASK_READ_CUSTOM_TIMESTAMP_FIELD3,
			IPermission.TASK_READ_CUSTOM_TIMESTAMP_FIELD4, IPermission.TASK_READ_CUSTOM_TIMESTAMP_FIELD5,
			IPermission.TASK_READ_CUSTOM_VAR_CHAR_FIELD1, IPermission.TASK_READ_CUSTOM_VAR_CHAR_FIELD2,
			IPermission.TASK_READ_CUSTOM_VAR_CHAR_FIELD3, IPermission.TASK_READ_CUSTOM_VAR_CHAR_FIELD4,
			IPermission.TASK_READ_CUSTOM_VAR_CHAR_FIELD5, IPermission.TASK_WRITE_CUSTOM_DECIMAL_FIELD1,
			IPermission.TASK_WRITE_CUSTOM_DECIMAL_FIELD2, IPermission.TASK_WRITE_CUSTOM_DECIMAL_FIELD3,
			IPermission.TASK_WRITE_CUSTOM_DECIMAL_FIELD4, IPermission.TASK_WRITE_CUSTOM_DECIMAL_FIELD5,
			IPermission.TASK_WRITE_CUSTOM_TIMESTAMP_FIELD1, IPermission.TASK_WRITE_CUSTOM_TIMESTAMP_FIELD2,
			IPermission.TASK_WRITE_CUSTOM_TIMESTAMP_FIELD3, IPermission.TASK_WRITE_CUSTOM_TIMESTAMP_FIELD4,
			IPermission.TASK_WRITE_CUSTOM_TIMESTAMP_FIELD5, IPermission.TASK_WRITE_CUSTOM_VAR_CHAR_FIELD1,
			IPermission.TASK_WRITE_CUSTOM_VAR_CHAR_FIELD2, IPermission.TASK_WRITE_CUSTOM_VAR_CHAR_FIELD3,
			IPermission.TASK_WRITE_CUSTOM_VAR_CHAR_FIELD4, IPermission.TASK_WRITE_CUSTOM_VAR_CHAR_FIELD5).collect(
			Collectors.collectingAndThen(Collectors.toSet(), Collections::unmodifiableSet));

	public static final Set<IPermission> DOCUMENT_PERMISSIONS = Stream.of(IPermission.DOCUMENT_READ,
			IPermission.DOCUMENT_WRITE, IPermission.DOCUMENT_OF_INVOLVED_CASE_READ,
			IPermission.DOCUMENT_OF_INVOLVED_CASE_WRITE).collect(
			Collectors.collectingAndThen(Collectors.toSet(), Collections::unmodifiableSet));

	private PermissionService() {}

	public static void setPermissionsForUserRole(Set<IPermission> permissions, IRole role) {
		permissions.forEach(p -> Ivy.wf().getApplication().getSecurityDescriptor().grantPermission(p, role));
	}
}
